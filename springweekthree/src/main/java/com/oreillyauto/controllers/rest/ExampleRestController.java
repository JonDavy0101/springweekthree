package com.oreillyauto.controllers.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.oreillyauto.domain.interns.Intern;
import com.oreillyauto.service.MappingsService;
import com.oreillyauto.util.RestHelper;

@RestController
public class ExampleRestController {

	@Autowired
	MappingsService mappingService;
	
    @GetMapping(value = "/rest/interns", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Intern> getAllInterns() {
    	return mappingService.getInterns();
    }
    
    @PostMapping(value = "/rest/interns/v1", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getAllInternsV1(@RequestHeader(value="Authorization") String authString) { 
    	
        if (!RestHelper.isUserAuthenticated(authString)){
            return "{\"error\":\"User not authenticated\"}";
        }

        return mappingService.getInterns();
    }
    
}
