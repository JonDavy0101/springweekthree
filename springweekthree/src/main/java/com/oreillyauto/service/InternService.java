package com.oreillyauto.service;

import java.util.List;
import com.twilio.rest.video.v1.Room;

public interface InternService {
	public List<Room> findInternsWithLastName(String lastName);
}