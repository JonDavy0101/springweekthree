package com.oreillyauto.domain.interns;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="BADGES")
public class Badge implements Serializable {
	private static final long serialVersionUID = -5996834027120348470L;
    public Badge() {}
    
    public Badge(Integer issuer) {
		super();
		this.issuer = issuer;
	}

	public Badge(Integer issuer, Intern intern) {
		super();
		this.issuer = issuer;
		this.intern = intern;
	}

	@Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "badge_id", columnDefinition = "INTEGER")
    private Integer badgeId;

    @Column(name = "issuer", columnDefinition = "INTEGER")
	private Integer issuer;

    @JsonIgnore
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "intern_id", referencedColumnName = "intern_id")
    private Intern intern;
    
	public Integer getBadgeId() {
		return badgeId;
	}

	public void setBadgeId(Integer badgeId) {
		this.badgeId = badgeId;
	}

	public Integer getIssuer() {
		return issuer;
	}

	public void setIssuer(Integer issuer) {
		this.issuer = issuer;
	}

	public Intern getIntern() {
		return intern;
	}

	public void setIntern(Intern intern) {
		this.intern = intern;
	}

	@Override
	public String toString() {
		return "Badge [badgeId=" + badgeId + ", issuer=" + issuer + "]";
	}  
    
}
