package com.oreillyauto.domain.university;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "course")
public class Course implements Serializable {
    private static final long serialVersionUID = -8221336284781448020L;

    @Id
    @Column(name = "course_id")
    private Integer courseId;
    
    @Column(name = "course_name")
    private String courseName;  
    
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "course")
    private List<University> universityList;

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public List<University> getUniversityList() {
		return universityList;
	}

	public void setUniversityList(List<University> universityList) {
		this.universityList = universityList;
	}

	@Override
	public String toString() {
		return "Course [courseGuid=" + courseId + ", courseName=" + courseName + "]";
	}

}
