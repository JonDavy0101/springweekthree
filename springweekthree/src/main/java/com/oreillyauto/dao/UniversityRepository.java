package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.UniversityRepositoryCustom;
import com.oreillyauto.domain.university.University;

/*
 *  CrudRepository<Example, Integer>  <== IMPORTANT: The integer is the datatype of your GUID (PK) on your table  
 */ 
public interface UniversityRepository extends CrudRepository<University, Integer>, UniversityRepositoryCustom {
    
    // Add your interface methods here (that ARE Spring Data methods)
    
}
