package com.oreillyauto.dao.custom;


import java.util.Map;

import com.oreillyauto.domain.foo.Clazz;

public interface ClassRepositoryCustom {    
	public Map<String,Clazz> getClasses();
}
