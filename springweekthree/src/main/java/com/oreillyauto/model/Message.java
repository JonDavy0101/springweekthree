package com.oreillyauto.model;

import java.util.ArrayList;
import java.util.List;

import com.oreillyauto.domain.interns.Intern;

public class Message {
	private String messageType;
	private String message;
	private List<Intern> internList = new ArrayList<Intern>();
	
	public Message() {
	}
	
	public Message(String messageType, String message) {
		super();
		this.messageType = messageType;
		this.message = message;
	}

	public Message(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public List<Intern> getInternList() {
		return internList;
	}

	public void setInternList(List<Intern> internList) {
		this.internList = internList;
	}
	
}
